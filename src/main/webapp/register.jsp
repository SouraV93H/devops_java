<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Signin Template · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
<link href="./assets/dist/css/bootstrap.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="./assets/dist/css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin" action="Register" method="post">
  <img class="mb-4" src="./assets/brand/Start.PNG" alt="" width="72" height="72">
  <h1 class="h3 mb-3 font-weight-normal">Forget Password</h1>
  <label for="inputEmail" class="sr-only">First Name</label>
  <input type="email" id="inputEmail" class="form-control" placeholder="Enter First Name" name="firstname" required autofocus><br>
  <label for="inputEmail" class="sr-only">Last Name</label>
  <input type="email" id="inputEmail" class="form-control" placeholder="Enter Lastt Name" name="lastname" required ><br>
  
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" id="inputPassword" class="form-control" placeholder="Enter Password" name="pass" required><br>

  <label for="inputPassword" class="sr-only">Confirm Password</label>
  <input type="password" id="inputPassword" class="form-control" placeholder="Enter ConfirmPassword" name="confirmpass" required><br>
  
  <label for="inputEmail" class="sr-only">Email address</label>
  <input type="email" id="inputEmail" class="form-control" placeholder="Enter Email" name="email" required><br>

  <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
  <p class="mt-5 mb-3 text-muted">&copy; 2017-2020</p>
</form>
</body>
</html>
